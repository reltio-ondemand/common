package com.reltio.lca;

import com.fasterxml.reltio.jackson.databind.JsonNode;
import com.fasterxml.reltio.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import com.reltio.lifecycle.server.core.services.LifeCycleHook;
import com.reltio.lifecycle.test.LifecycleExecutor;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.HashMap;

import com.reltio.lca.AttributeCheck;
import com.reltio.lca.AWSConfigFile;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AttributeCheckTest {
	
	public AttributeCheckTest() throws IOException {
	} 
	
    private LifecycleExecutor executor = new LifecycleExecutor();
    private AttributeCheck handler = new AttributeCheck();

    private AWSConfigFile configValues = new AWSConfigFile();
    HashMap<String, String> hmTest = configValues.getConfigFile()  ;
    String blankAttrTest = hmTest.get("NOT_BLANK");
    String defaultValueTest = hmTest.get("DEFAULT");
    String lengthAttrTest = hmTest.get("LENGTH_ATTR");
    String lengthTest = hmTest.get("LENGTH");
    int lengthInt = Integer.parseInt(lengthTest);
    String rangeAttrTest= hmTest.get("RANGE_ATTR");
    String maxTest = hmTest.get("MAX");
    String minTest = hmTest.get("MIN");
    int maxInt = Integer.parseInt(maxTest);
    int minInt = Integer.parseInt(minTest);
    String capAttrTest = hmTest.get("CAP_ATTR");
    String dateAttrTest = hmTest.get("DATE_ATTR");
    String dateValueTest = hmTest.get("DATE");

//    @Test
//    public void attributeCheckTest() throws URISyntaxException, IOException {
//        //LifecycleExecutor executor = new LifecycleExecutor();
//       // AttributeCheck handler = new AttributeCheck();
//        String input = Files.toString(new File(getClass().getResource("/input.json").toURI()), Charset.defaultCharset());
//
//        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);
//
//        ObjectMapper objectMapper = new ObjectMapper();
//        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
//        String name = jsonNode.get("object").get("attributes").get("Industry").elements().next().get("value").asText();
//        //assertEquals("Wrong value is in this attribute!", name);
//        assertTrue(name != null);
//    }
    @Test
    public void attributeNullTest() throws URISyntaxException, IOException {
        String input = Files.toString(new File(getClass().getResource("/input_null_values.json").toURI()), Charset.defaultCharset());
        System.out.println("JUnit time");
        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        String name = jsonNode.get("object").get("attributes").get(blankAttrTest).elements().next().get("value").asText();
        assertEquals(defaultValueTest, name);
    }
   @Test
    public void attributeNotNullTest() throws URISyntaxException, IOException {
        String input = Files.toString(new File(getClass().getResource("/input.json").toURI()), Charset.defaultCharset());

        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        String name = jsonNode.get("object").get("attributes").get(blankAttrTest).elements().next().get("value").asText();
        assertTrue(name != defaultValueTest);
    }
    @Test
    public void lengthNullTest() throws URISyntaxException, IOException {
        String input = Files.toString(new File(getClass().getResource("/input_null_values.json").toURI()), Charset.defaultCharset());
        
        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        try{
        	@SuppressWarnings("unused")
            String name = jsonNode.get("object").get("attributes").get(lengthAttrTest).elements().next().get("value").asText();
        }catch (NullPointerException e) {
        	String name = null;
        	assertTrue(name == null);
        }    }
    @Test
    public void lengthNotNullTest() throws URISyntaxException, IOException {

        String input = Files.toString(new File(getClass().getResource("/input.json").toURI()), Charset.defaultCharset());

        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        String name = jsonNode.get("object").get("attributes").get(lengthAttrTest).elements().next().get("value").asText();
        lengthInt = lengthInt - 1; // because arrays start at 0
        assertTrue(name.length() == lengthInt);
    }
    @Test
    public void rangeNullTest() throws URISyntaxException, IOException {
        String input = Files.toString(new File(getClass().getResource("/input_null_values.json").toURI()), Charset.defaultCharset());
        
        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        try{
        	@SuppressWarnings("unused")
            String name = jsonNode.get("object").get("attributes").get(rangeAttrTest).elements().next().get("value").asText();
        }catch (NullPointerException e) {
        	String name = null;
        	assertTrue(name == null);
        }
    }
    @Test
    public void rangeNotNullTest() throws URISyntaxException, IOException {
        String input = Files.toString(new File(getClass().getResource("/input.json").toURI()), Charset.defaultCharset());

        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        String name = jsonNode.get("object").get("attributes").get(rangeAttrTest).elements().next().get("value").asText();
        int attrRange = Integer.parseInt(name);
        assertTrue(attrRange == minInt || maxInt == attrRange);
    }
    @Test
    public void capNullTest() throws URISyntaxException, IOException {
        String input = Files.toString(new File(getClass().getResource("/input_null_values.json").toURI()), Charset.defaultCharset());
        
        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        try{
        	@SuppressWarnings("unused")
            String name = jsonNode.get("object").get("attributes").get(capAttrTest).elements().next().get("value").asText();
        }catch (NullPointerException e) {
        	String name = null;
        	assertTrue(name == null);
        }
        
    }
    @Test
    public void capNotNullTest() throws URISyntaxException, IOException {
        String input = Files.toString(new File(getClass().getResource("/input.json").toURI()), Charset.defaultCharset());

        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        String name = jsonNode.get("object").get("attributes").get(capAttrTest).elements().next().get("value").asText();
        char first = name.charAt(0);
        assertTrue(Character.isUpperCase(first));
    }
   @Test
    public void dateNullTest() throws URISyntaxException, IOException {
        String input = Files.toString(new File(getClass().getResource("/input_null_values.json").toURI()), Charset.defaultCharset());
        
        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        try{
        	@SuppressWarnings("unused")
            String name = jsonNode.get("object").get("attributes").get(dateAttrTest).elements().next().get("value").asText();
        }catch (NullPointerException e) {
        	String name = null;
        	assertTrue(name == null);
        }
    }
   @Test
    public void dateNotNullTest() throws URISyntaxException, IOException {
        String input = Files.toString(new File(getClass().getResource("/input.json").toURI()), Charset.defaultCharset());

        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        String name = jsonNode.get("object").get("attributes").get(dateAttrTest).elements().next().get("value").asText();

        assertTrue(name.equals(dateValueTest) );
    }
//    @Test
//    public void attributeCheckTest() throws URISyntaxException, IOException {
//        LifecycleExecutor executor = new LifecycleExecutor();
//        AttributeCheck handler = new AttributeCheck();
//        String input = Files.toString(new File(getClass().getResource("/input.json").toURI()), Charset.defaultCharset());
//
//        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);
//
//        ObjectMapper objectMapper = new ObjectMapper();
//        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
//        String name = jsonNode.get("object").get("attributes").get("Industry").elements().next().get("value").asText();
//        assertEquals("Wrong value is in this attribute!", name);
//        //assertTrue(name != null);
//    }

}
