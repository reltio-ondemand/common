package com.reltio.lca;

import com.reltio.lca.AWSConfigFile;
import com.reltio.lifecycle.framework.*;
import com.reltio.lifecycle.lambda.LifeCycleActionHandler;

import org.joda.time.DateTime;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class AttributeCheck extends LifeCycleActionHandler {

    @Override
    public ILifeCycleObjectData beforeSave(IReltioAPI reltioAPI, ILifeCycleObjectData data) {
    	IObject object = data.getObject();
        IAttributes attributes = object.getAttributes();
        
        
        try { 
        	System.out.println("Attribute check initialized");

        	//Gets the properties file hosted in S3
        	AWSConfigFile configValues = new AWSConfigFile();
        	HashMap<String, String> hm1 = configValues.getConfigFile();
        	
        	//Assign the values in the HashMap to Strings
        	String blankAttribute = hm1.get("NOT_BLANK");
        	String defaultValue = hm1.get("DEFAULT");
        	String lengthAttribute = hm1.get("LENGTH_ATTR");
        	String length = hm1.get("LENGTH");
        	String rangeAttribute= hm1.get("RANGE_ATTR");
        	String max = hm1.get("MAX");
        	String min = hm1.get("MIN");
        	String capAttribute = hm1.get("CAP_ATTR");
        	String dateAttribute = hm1.get("DATE_ATTR");
        	String dateValue = hm1.get("DATE");
        	
        	//This block of code replaces an Attribute values with another 
            List<IAttributeValue> cannotBeBlank = attributes.getAttributeValues(blankAttribute);
//            List<IAttributeValue> replaceBlankWithThis = attributes.getAttributeValues(defaultValue);
            if (cannotBeBlank.isEmpty()) {

                //ISimpleAttributeValue replaceValue = (ISimpleAttributeValue) replaceBlankWithThis.get(0);
                //String replace = replaceValue.getStringValue();
                ISimpleAttributeValue newAttribute = attributes.createSimpleAttributeValue(blankAttribute).value(defaultValue).build();
                attributes.addAttributeValue(newAttribute);
            }
            
            //This Block of code checks the length of a variable
            List<IAttributeValue> lengthLimit = attributes.getAttributeValues(lengthAttribute);
            if(!lengthLimit.isEmpty()){
                ISimpleAttributeValue valueLimit = (ISimpleAttributeValue) lengthLimit.get(0);
                String strLimit = valueLimit.getStringValue();
                
                int ruleLength = Integer.parseInt(length);
                ruleLength = ruleLength - 1; //because arrays start at 0 and I am assuming who ever uses the property file does not know this
                
                if(strLimit.length() > ruleLength){
                	String truncValue = strLimit.substring(0,ruleLength);
                	attributes.removeAttributeValue(valueLimit);
                	ISimpleAttributeValue shortValue = attributes.createSimpleAttributeValue(lengthAttribute).value(truncValue).build();
                    attributes.addAttributeValue(shortValue);
                }
            }

            //This Block checks to see if the an Attributes Value is within a certain range
            List<IAttributeValue> uncheckedVal = attributes.getAttributeValues(rangeAttribute);
           
            if(!uncheckedVal.isEmpty()){
	            ISimpleAttributeValue unchkdValSAV = (ISimpleAttributeValue) uncheckedVal.get(0);
	            String strUncheckedVal = unchkdValSAV.getStringValue();
	            
	            int intUncheckedVal = Integer.parseInt(strUncheckedVal);
	            int minRule = Integer.parseInt(min);
	            int maxRule = Integer.parseInt(max);
	            
	            if(intUncheckedVal < minRule){
	            	attributes.removeAttributeValue(unchkdValSAV);
	            	ISimpleAttributeValue minValue = attributes.createSimpleAttributeValue(rangeAttribute).value(min).build();
	                attributes.addAttributeValue(minValue);
	            }
	            if(intUncheckedVal > maxRule){
	            	attributes.removeAttributeValue(unchkdValSAV);
	            	ISimpleAttributeValue maxValue = attributes.createSimpleAttributeValue(rangeAttribute).value(max).build();
	                attributes.addAttributeValue(maxValue);
	            }
            }
            //This Block of code checks to see it the first index of a value is capitalized
            List<IAttributeValue> checkCaps = attributes.getAttributeValues(capAttribute);
            if(!checkCaps.isEmpty()){
	            ISimpleAttributeValue checkCapsSAV = (ISimpleAttributeValue) checkCaps.get(0);
	            String strCheckCaps = checkCapsSAV.getStringValue();
	            char first = strCheckCaps.charAt(0);
	            
	            if(!Character.isUpperCase(first)){
	                char upper = Character.toUpperCase(first);
	                strCheckCaps = strCheckCaps.substring(1);
	                strCheckCaps = upper + strCheckCaps;
	                
	                attributes.removeAttributeValue(checkCapsSAV);
	            	ISimpleAttributeValue fixedString = attributes.createSimpleAttributeValue(capAttribute).value(strCheckCaps).build();
	                attributes.addAttributeValue(fixedString);
	            }
            }
            //The Block of code checks to make sure that the date is not after today
            List<IAttributeValue> dateList = attributes.getAttributeValues(dateAttribute);
            if(!dateList.isEmpty()) {
                ISimpleAttributeValue dateSAV = (ISimpleAttributeValue) dateList.get(0);
                String entityDateString = dateSAV.getStringValue();
                SimpleDateFormat format = new SimpleDateFormat("y-M-d");
                Date entityDateValue = null;
                Date propDateValue = null;

                entityDateValue = format.parse(entityDateString);
                propDateValue = format.parse(dateValue);

                DateTime edv = new DateTime(entityDateValue);
                DateTime pdv = new DateTime(propDateValue);
                if(edv.isAfter(pdv)){
                	attributes.removeAttributeValue(dateSAV);
                    ISimpleAttributeValue defaultDateValue = attributes.createSimpleAttributeValue(dateAttribute).value(dateValue).build();
                    attributes.addAttributeValue(defaultDateValue);
                }
            }

            return data;
        	} catch (Exception ex) {
            reltioAPI.logError(ex.getMessage());
            throw new RuntimeException("AttributeCheck: LCA invocation failed.");
        }
    }
}
