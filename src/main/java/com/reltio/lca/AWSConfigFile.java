package com.reltio.lca;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

//import org.apache.log4j.Logger;

//import com.amazonaws.AmazonClientException;
//import com.amazonaws.AmazonServiceException;
//import com.amazonaws.auth.AWSStaticCredentialsProvider;
//import com.amazonaws.auth.BasicAWSCredentials;
//import com.amazonaws.services.s3.AmazonS3;
//import com.amazonaws.services.s3.AmazonS3ClientBuilder;
//import com.amazonaws.services.s3.model.GetObjectRequest;
//import com.amazonaws.services.s3.model.S3Object;

public class AWSConfigFile {
	//Constants
//	private final static String ACCESS_KEY_ID = "#################";
//	private final static String SECRET_ACCESS_KEY = "*****************************************";
//	private final static String BUCKET_NAME = "reltio-tc-test"; 
//	private final static String KEY        = "lca.properties"; 
//	Logger
//	private static Logger log = Logger.getLogger(AWSConfigFile.class.getName());
//	AWS Credentials
//    BasicAWSCredentials creds = new BasicAWSCredentials(ACCESS_KEY_ID, SECRET_ACCESS_KEY); 
    
	public HashMap<String, String> getConfigFile() throws IOException {
	   //Logging in to S3 Bucket
//       AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion("us-east-1").withCredentials(new AWSStaticCredentialsProvider(creds)).build();

        try{
        	System.out.println("Getting the configuration file");
//        	S3Object s3object = s3Client.getObject(new GetObjectRequest(BUCKET_NAME, KEY));
//        	System.out.println("Content-Type: " + s3object.getObjectMetadata().getContentType());
            Properties prop = new Properties();
            
            //Used when testing locally
        	String propFileName = "lca.properties";
        	InputStream inputStream;
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

//        	InputStream input = s3object.getObjectContent();

        	//load a properties file
//        	prop.load(input);
        	//Used when testing locally
        	prop.load(inputStream);
    		HashMap<String, String> hm1 = new HashMap<String, String>();
    		//getting property files attributes

        		hm1.put("NOT_BLANK", prop.getProperty("BLANK_ATTRIBUTE"));
        		hm1.put("DEFAULT", prop.getProperty("DEFAULT_VALUE"));
        		hm1.put("LENGTH_ATTR", prop.getProperty("LENGTH_ATTRIBUTE"));
        		hm1.put("LENGTH", prop.getProperty("LENGTH"));
        		hm1.put("RANGE_ATTR", prop.getProperty("RANGE_ATTRIBUTE"));
        		hm1.put("MAX", prop.getProperty("MAX"));
        		hm1.put("MIN", prop.getProperty("MIN"));
        		hm1.put("CAP_ATTR", prop.getProperty("CAPITALIZE_ATTRIBUTE"));
        		hm1.put("DATE_ATTR", prop.getProperty("DATE_ATTRIBUTE"));
        		hm1.put("DATE", prop.getProperty("DATE"));
    		return hm1;
        	
       //EXCEPTIONS*************************** 	
//        } catch (AmazonServiceException ase) {
//        	System.out.println("Caught an AmazonServiceException, which" +
//            		" means your request made it " +
//                    "to Amazon S3, but was rejected with an error response" +
//                    " for some reason.");
//        	System.out.println("Error Message:    " + ase.getMessage());
//        	System.out.println("HTTP Status Code: " + ase.getStatusCode());
//        	System.out.println("AWS Error Code:   " + ase.getErrorCode());
//        	System.out.println("Error Type:       " + ase.getErrorType());
//        	System.out.println("Request ID:       " + ase.getRequestId());
//        } catch (AmazonClientException ace) {
//        	System.out.println("Caught an AmazonClientException, which means"+
//            		" the client encountered " +
//                    "an internal error while trying to " +
//                    "communicate with S3, " +
//                    "such as not being able to access the network.");
//        	System.out.println("Error Message: " + ace.getMessage());
//        }
      //Used when testing locally       

//	return null;
        }finally{
        	
        }
	}
}