# Quick Start 
The quick start(H2) is akin to the read me file for a utility. Here you would outline the build directions, dependencies, parameters and execution instructions as H3 headers. Make sure to place any code or mappings in a code box.

##Building
Use this section to detail how to build your tool and the location of the main method.

The main method of the application is at the following path:
**./src/main/java/com/buildpath **

##Dependencies 

1. dependency 1.1.1
2. dependency 2.2.2


##Parameters File Example
If your tool utilizes a paramter file place an example in a code box. Otherwise remove this section.

```
#!paintext
API_URL= http://google.com/
USERNAME= user@reltio.com
PASSWORD=*****
AUTH_URL=https://auth.reltio.com/oauth/token
INTERVAL=60

```
##Executing
Use this section to explain how to call your newly built tool.

Command to start the utility.
```
#!plaintext

java -jar utlity.jar > log.txt

```