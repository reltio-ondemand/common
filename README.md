
#Sample Readme Example

## Description
The purpose of this example is to give you a template from which to create future readme files on ROCS. Be sure to set your landing page for the repository as overview under the "Repository Details tab".

The Description tab is a H2 header that gives a brief high level overview of your tool. The disclaimer and Contributing headers are also H2 and are always the same so just copy and paste the text from there.


##Change Log
Change log is a h2 header that should be inside of a code box. Follow the format below and always place the most recent update on top.

```
#!plaintext



Last Update Date: 05/05/2005
Version: 1.0.0
Description: Initial version
```
##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/example/src/8317e11936aca62e637a45aeb51cce58b46aee73/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
#!plaintext
Copyright (c) 2017 Reltio

 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/example/src/8317e11936aca62e637a45aeb51cce58b46aee73/QuickStart.md?at=master&fileviewer=file-view-default).


